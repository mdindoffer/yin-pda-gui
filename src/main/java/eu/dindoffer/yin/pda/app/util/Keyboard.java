package eu.dindoffer.yin.pda.app.util;

import java.util.Arrays;
import java.util.List;

/**
 * Statická trieda reprezentujúca celú klaviatúru
 *
 * @author Martin Dindoffer
 */
public class Keyboard {

    private static final double DEFAULT_REF_FREQ = 440;
    private static final Tone[] TONE_ARR = new Tone[128];//midi štandard má 128 nôt

    private static TuningSystem currentSystem;

    static {
        setEqualTemperament(DEFAULT_REF_FREQ);
    }

    //static class
    private Keyboard() {
    }

    /**
     * Vráti pole tónov tvoriacich klaviatúru
     *
     * @return pole tónov
     */
    public static Tone[] getPoleTonov() {
        return TONE_ARR;
    }

    /**
     * Nastaví systém ladenia na rovnomerne temperované ladenie s danou referenčnou frekvenciou (A4).
     *
     * @param paRefFreq referenčná frekvencia tónu A4
     */
    public static void setEqualTemperament(double paRefFreq) {
        TONE_ARR[0] = new Tone("C-1", paRefFreq * Math.pow(2, -5.75), 0);
        List<ToneNames> l = Arrays.asList(ToneNames.values());
        for (int i = 1; i < 128; i++) {
            TONE_ARR[i] = new Tone(l.get(i % 12).toString() + (i / 12 - 1), TONE_ARR[i - 1].getFrekvencia() * Math.pow(2, (1 / 12.0)), i);
        }
        currentSystem = TuningSystem.EQUAL_TEMPERAMENT;
    }

    /**
     * Nastaví systém ladenia na Pythagorejské ladenie s danou referenčnou frekvenciou daného základného tónu.
     *
     * @param paRefFreq referenčná frekvencia základného tónu
     * @param paBaseTone základný tón, od ktorého sa vytvorí Pythagorova 12-tónová stupnica
     */
    public static void setPythagoreanTuning(double paRefFreq, int paBaseTone) {
        justIntonationSkeleton(paRefFreq, paBaseTone, 2 / 3.0);
        currentSystem = TuningSystem.PYTHAGOREAN_TUNING;
    }

    /**
     * Nastaví systém ladenia na Quarter-comma meantone ladenie s danou referenčnou frekvenciou daného základného tónu.
     *
     * @param paRefFreq referenčná frekvencia základného tónu
     * @param paBaseTone základný tón, od ktorého sa vytvorí 12-tónová stupnica
     */
    public static void setQuarterCommaMeantoneTuning(double paRefFreq, int paBaseTone) {
        justIntonationSkeleton(paRefFreq, paBaseTone, Math.pow(5, -1 / 4.0));
        currentSystem = TuningSystem.QUARTERCOMMA_MEANTONE;
    }

    private static void justIntonationSkeleton(double paRefFreq, int paBaseTone, double paAscRatio) {
        double tempRef = paRefFreq / 32; //reference frequency at the lowest octave
        List<ToneNames> l = Arrays.asList(ToneNames.values());
        int[] intervalSequence = {6, 11, 4, 9, 2, 7, 0, 5, 10, 3, 8, 1}; // size of subsequent calculated intervals in semitones
        double temp;
        for (int i = 0; i < 12; i++) { //cycle for lowest octave
            temp = Math.pow(paAscRatio, i - 6) * tempRef;
            
            int toneNum = (paBaseTone + intervalSequence[i]) % 12;
            while (temp > (tempRef * 2)) {
                temp /= 2;
            }
            while (temp < (tempRef / 2)) {
                temp *= 2;
            }
            if(toneNum<paBaseTone && temp>tempRef){
                temp/=2;
            }
            if(toneNum>paBaseTone && temp<tempRef){
                temp*=2;
            }
            TONE_ARR[toneNum] = new Tone(l.get(toneNum) + "-1", temp, toneNum);
        }
        for (int i = 12; i < 128; i++) { // fill remaining octaves
            TONE_ARR[i] = new Tone(l.get(i % 12).toString() + (i / 12 - 1), TONE_ARR[i - 12].getFrekvencia() * 2, i);
        }
    }
}
