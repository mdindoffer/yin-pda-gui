package eu.dindoffer.yin.pda.app.evaluation;

import eu.dindoffer.yin.pda.app.util.Note;
import eu.dindoffer.yin.pda.app.util.Tone;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;
import eu.dindoffer.yin.pda.app.util.Keyboard;

/**
 * Statický dekodér pre MIDI súbory
 *
 * @author Martin Dindoffer
 */
public final class MIDIDecoder {

    private static final int SET_TEMPO = 0x51;

    //statická trieda
    private MIDIDecoder() {
    }

    /**
     * Dekóduje zadaný SMF súbor typu 1. Podporuje všetky typy delenia času SMF súborov (PPQ a všetky SMPTE). Ako zdroj nôt berie track č. 1.
     * Polyfónii predchádza pomocou predčasného ukončenia noty, t.j. nová nota má vždy prednosť. Do úvahy berie iba prvú definíciu tempa v tracku 0.
     * Takisto neberie do úvahy signál Hold Pedal. Pre správnu funkčnosť teda vyžaduje:
     * <ol>
     * <li>dodržiavanie konvencie, kde sa do tracku 0 ukladá tempo</li>
     * <li>použitie iba jedného tempa pre celú sekvenciu</li>
     * <li>uloženie nôt na dekódovanie do tracku č. 1</li>
     * <li>nepoužívanie pedálu (signál Hold Pedal)</li>
     * </ol>
     *
     * @param paFile súbor na dekódovanie
     * @return zoznam nôt
     * @throws IOException ak nastane I/O výnimka
     * @throws InvalidMidiDataException ak súbor nie je validný SMF súbor typu 1, alebo nemá aspoň 2 tracky
     */
    public static LinkedList<Note> decode(File paFile) throws IOException, InvalidMidiDataException {
        if (MidiSystem.getMidiFileFormat(paFile).getType() != 1) {
            throw new InvalidMidiDataException("SMF súbor nie je typu 1");
        }
        Sequence sequence = MidiSystem.getSequence(paFile);
        Track[] tracks = sequence.getTracks();
        if (tracks.length < 2) {
            throw new InvalidMidiDataException("Súbor nemá dostatočný počet trackov.");
        }

        float typDelenia = sequence.getDivisionType();
        double rozlisenie = sequence.getResolution();//buď počet tickov za štvrťovú notu, alebo za frame

        LinkedList<Note> noty = new LinkedList<>();
        final Tone[] poleTonov = Keyboard.getPoleTonov();

        int tempo = 500000;//tempo je v mikrosekundách za štvrťovú notu, default je 500000
        //najskôr prebehneme track0 a zistíme tempo (prvé, ostatné ignorujeme)
        for (int i = 0; i < tracks[0].size(); i++) {
            MidiEvent event = tracks[0].get(i);
            MidiMessage message = event.getMessage();
            if (message instanceof MetaMessage) {
                MetaMessage mm = (MetaMessage) message;
                if (mm.getType() == SET_TEMPO) {
                    byte[] data = mm.getData();
                    tempo = (data[0] & 0xff) << 16 | (data[1] & 0xff) << 8 | (data[2] & 0xff);
                    break;
                }
            }
        }

        //vypočítame dĺžku ticku v us
        double dlzkaTicku;
        if (typDelenia == Sequence.PPQ) {
            dlzkaTicku = tempo / rozlisenie;
        } else {
            dlzkaTicku = 1000000 / (rozlisenie * typDelenia);
        }

        int predch = -1;
        for (int i = 0; i < tracks[1].size(); i++) {
            MidiEvent event = tracks[1].get(i);
            MidiMessage message = event.getMessage();
            if (message instanceof ShortMessage) {
                ShortMessage sm = (ShortMessage) message;
                if (sm.getCommand() == ShortMessage.NOTE_ON && sm.getData2() != 0) {//zapnutie novej noty
                    if (predch == -1) {// čisté zapnutie
                        predch = sm.getData1();//tón
                        noty.add(new Note(poleTonov[predch], event.getTick() * dlzkaTicku));
                    } else {// špinavé - (predchádzajúca nota ešte oficiálne nedohrala), treba vypnúť predchádzajúcu.
                        if (event.getTick() * dlzkaTicku - noty.peekLast().getUsStart() > 1) { // ak by sme mali nastaviť predchádzajúcej note dĺžku aspoň 1us
                            noty.peekLast().setUsEnd(event.getTick() * dlzkaTicku - 1); // tak ju nastavíme
                        } else {
                            noty.removeLast(); //inak ju zmažeme, lebo by mala nulovú dĺžku
                        }
                        predch = sm.getData1();
                        noty.add(new Note(poleTonov[predch], event.getTick() * dlzkaTicku));
                    }
                } else if ((sm.getCommand() == ShortMessage.NOTE_OFF && sm.getData1() == predch)//čisté vypnutie noty
                        || (sm.getCommand() == ShortMessage.NOTE_ON && sm.getData1() == predch && sm.getData2() == 0)) {//alternatívne čisté vypnutie
                    noty.peekLast().setUsEnd(event.getTick() * dlzkaTicku);
                    predch = -1;
                }
            }
        }
        return noty;
    }
}
