package eu.dindoffer.yin.pda.app.util;

/**
 * Zoznam podporovaných systémov ladenia.
 * @author Martin Dindoffer
 */
public enum TuningSystem {

    EQUAL_TEMPERAMENT("Equal temperament"), PYTHAGOREAN_TUNING("Pythagorean tuning"), QUARTERCOMMA_MEANTONE("Quarter-comma meantone");

    private final String stringRep;

    private TuningSystem(String paStringRep) {
        this.stringRep = paStringRep;
    }
    
    @Override
    public String toString(){
        return stringRep;
    }
}
