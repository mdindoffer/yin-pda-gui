package eu.dindoffer.yin.pda.app.evaluation;

import eu.dindoffer.yin.pda.app.util.Note;
import eu.dindoffer.yin.pda.app.util.Tone;
import java.util.Arrays;
import java.util.LinkedList;
import eu.dindoffer.yin.pda.app.util.Keyboard;

/**
 * Statický evalvátor výsledkov algoritmu.
 *
 * @author Martin Dindoffer
 */
public class Evaluator {

    //statická trieda
    private Evaluator() {
    }

    /**
     * Ohodnotí úspešnosť algoritmu. Porovná hodnoty získané analýzou zvukového súboru so zoznamom
     * nôt. Frekvencie získané algoritmom prehlási za tóny, ku ktorým sú najbližšie. V prípade
     * nerozhodnosti sa výsledok v danom časovom okne automaticky považuje za nesprávny. Ak
     * frekvencia presahuje rozsah zadefinovaných tónov, považuje sa za krajný tón v prípade, že je
     * k nemu bližšie než k ďalšiemu tónu (t.j. G#0 pre nízke a C#8 pre vysoké frekvencie). Sledujú
     * sa iba výsledky v časových oknách, v ktorých sa nachádzajú noty.
     *
     * @param paNoty zoznam nôt
     * @param paFrekvencie výsledné frekvencie po analýze zvukového súboru
     * @param paTaumax hodnota Taumax použitá pri analýze
     * @param paSampleRate vzorkovacia frekvencia zvukového súboru
     * @return percentuálna úspešnosť v rozmedzí 0 až 1
     */
    public static double evaluate(LinkedList<Note> paNoty, double[] paFrekvencie, int paTaumax, float paSampleRate) {
        double casOkna = (1000000 * paTaumax / paSampleRate);//čas okna v us
        Tone[] tony = Keyboard.getPoleTonov();
        double[] frTonov = new double[tony.length];
        for (int i = 0; i < tony.length; i++) {
            frTonov[i] = tony[i].getFrekvencia();
        }
        int zhoda = 0;
        int pocetMerani = 0;
        for (Note nota : paNoty) {
            int indexZaciatku = (int) (nota.getUsStart() / casOkna);//odpovedajúci index v poli frekvencií
            int indexKonca = (int) (nota.getUsEnd() / casOkna);
            for (int i = indexZaciatku; i <= indexKonca; i++) {
                if (i >= paFrekvencie.length) {//takto predídeme IndexOutOfBounds a môžeme použiť kratšie nahrávky než MIDI súbor
                    break;
                }
                pocetMerani++;
                int indexRet = Arrays.binarySearch(frTonov, paFrekvencie[i]);
                if (indexRet >= 0) {// ak sme sa náhodou trafili do presnej frekvencie tónu
                    zhoda++;
                } else {//inak zistíme kú ktorému tónu je frekvencia bližšie
                    if (indexRet != -1 && indexRet != -(tony.length + 1)) {//ak nie sme mimo rozsahu definovaných tónov
                        double prvyTonDif = Math.abs(tony[-(indexRet + 2)].getFrekvencia() - paFrekvencie[i]);
                        double druhyTonDif = Math.abs(tony[-(indexRet + 1)].getFrekvencia() - paFrekvencie[i]);
                        if (prvyTonDif < druhyTonDif) {
                            if (nota.getTon() == tony[-(indexRet + 2)]) {
                                zhoda++;
                            }
                        } else if (druhyTonDif < prvyTonDif) {
                            if (nota.getTon() == tony[-(indexRet + 1)]) {
                                zhoda++;
                            }
                        }
                    }
                }
            }
        }
        return zhoda / (double) pocetMerani;
    }
}
