package eu.dindoffer.yin.pda.app.util;

/**
 * Note predstavuje tón hraný v nejakom čase s danou dĺžkou.
 *
 * @author Martin Dindoffer
 */
public class Note {

    private final double usStart;
    private double usEnd;
    private final Tone ton;

    /**
     * Vytvorí notu z daného tónu a počiatočného času.
     *
     * @param paTon tón ktorý nota predstavuje
     * @param paUsStart čas začiatku trvania noty v mikrosekundách
     */
    public Note(Tone paTon, double paUsStart) {
        this.usStart = paUsStart;
        this.ton = paTon;
    }

    /**
     * Nastaví koniec trvania noty.
     *
     * @param paUsEnd čas konca trvania noty v mikrosekundách
     */
    public void setUsEnd(double paUsEnd) {
        this.usEnd = paUsEnd;
    }

    /**
     * Vráti začiatok trvania noty.
     *
     * @return čas začiatku trvania noty v mikrosekundách
     */
    public double getUsStart() {
        return usStart;
    }

    /**
     * Vráti koniec trvania noty.
     *
     * @return čas konca trvania noty v mikrosekundách
     */
    public double getUsEnd() {
        return usEnd;
    }

    /**
     * Vráti tón, ktorý nota predstavuje.
     *
     * @return tón noty
     */
    public Tone getTon() {
        return ton;
    }

}
