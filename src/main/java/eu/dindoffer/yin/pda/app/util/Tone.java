package eu.dindoffer.yin.pda.app.util;

/**
 * Tón klaviatúry.
 * @author Martin Dindoffer
 */
public class Tone {

    private final String label;
    private final double frekvencia;
    private final int midiNumber;

    /**
     * Vytvorí tón s danou frekvenciou, názvom a MIDI označením.
     * @param paLabel názov tónu
     * @param paFrekvencia frekvencia tónu
     * @param paMidiNumber MIDI číslo tónu
     */
    public Tone(String paLabel, double paFrekvencia, int paMidiNumber) {
        this.label = paLabel;
        this.frekvencia = paFrekvencia;
        this.midiNumber = paMidiNumber;
    }

    /**
     * Vráti názov tónu v medzinárodnom značení.
     *
     * @return názov tónu
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Vráti frekvenciu tónu.
     *
     * @return frekvencia tónu
     */
    public double getFrekvencia() {
        return this.frekvencia;
    }
    
    /**
     * Vráti MIDI číslo tónu
     * @return Midi číslo tónu
     */
    public int getMidiNumber(){
        return this.midiNumber;
    }
}
