package eu.dindoffer.yin.pda.app.gui.rendering;

import eu.dindoffer.yin.pda.app.util.Note;
import java.awt.BorderLayout;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.jfree.data.Range;
import org.jfree.data.time.FixedMillisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import eu.dindoffer.yin.pda.app.util.Keyboard;

/**
 * GUI okno s grafom výsledkov
 *
 * @author Martin Dindoffer
 */
public class GraphWindow extends javax.swing.JFrame {

    private final JFreeChart chart;
    private ToneAxis tonovaOs;
    private NumberAxis ciselnaOs;

    /**
     * Vytvorí nové okno s grafom výsledkov analýzy.
     *
     * @param paFrekvencie frekvencie na vykreslenie
     * @param paNazov názov analyzovaného súboru
     * @param paTaumax hodnota Taumax
     * @param paSampleRate vzorkovacia frekvencia analyzovaného súboru
     */
    public GraphWindow(double[] paFrekvencie, String paNazov, int paTaumax, float paSampleRate) {
        initComponents();
        TimeSeriesCollection dataset = vytvorDataset(paFrekvencie, paTaumax, paSampleRate);
        chart = ChartFactory.createTimeSeriesChart("Priebeh F0 - " + paNazov + " (TauMax: " + paTaumax + ")", "Čas(mm:ss.SSS)", "Frekvencia(Hz)", dataset);
        vytvorChart();
    }

    /**
     * Vytvorí nové okno s grafom výsledkov analýzy, dekódovaného MIDI súboru a výsledkom evalvácie.
     *
     * @param paFrekvencie výsledky analýzy ako frekvencie na vykreslenie
     * @param paNazovZvuku názov analyzovaného súboru
     * @param paNazovMidi názov MIDI súboru
     * @param paNoty zoznam nôt z dekódovaného MIDI súboru
     * @param paTaumax hodnota Taumax
     * @param paSampleRate vzorkovacia frekvencia analyzovaného súboru
     * @param paZhoda výsledok evalvácie ako percentuálna zhoda, v rozsahu 0 až 1
     */
    public GraphWindow(double[] paFrekvencie, String paNazovZvuku, String paNazovMidi, LinkedList<Note> paNoty, int paTaumax, float paSampleRate, double paZhoda) {
        initComponents();
        lblZhoda.setText("Zhoda s MIDI dátami: " + String.format("%.3f", paZhoda * 100) + "%");
        TimeSeriesCollection dataset = vytvorDataset(paFrekvencie, paTaumax, paSampleRate);
        TimeSeries tsNoty = new TimeSeries("Noty z " + paNazovMidi);
        long predoslyKoniec = 0;
        for (Note nota : paNoty) {
            long zaciatok = (long) (nota.getUsStart() / 1000);
            if (predoslyKoniec < (zaciatok - 1)) {
                tsNoty.addOrUpdate(new FixedMillisecond(predoslyKoniec + 1), null);
            }
            tsNoty.addOrUpdate(new FixedMillisecond(zaciatok), nota.getTon().getFrekvencia());
            predoslyKoniec = (long) (nota.getUsEnd() / 1000);
            tsNoty.addOrUpdate(new FixedMillisecond(predoslyKoniec), nota.getTon().getFrekvencia());
        }
        dataset.addSeries(tsNoty);
        chart = ChartFactory.createTimeSeriesChart("Priebeh F0 - " + paNazovZvuku + " vs. " + paNazovMidi + " (TauMax: " + paTaumax + ")", "Čas(mm:ss.SSS)", "Frekvencia(Hz)", dataset);
        vytvorChart();
    }

    private TimeSeriesCollection vytvorDataset(double[] paFrekvencie, int paTaumax, float paSampleRate) {
        double casOkna = (1000 * paTaumax / paSampleRate);
        TimeSeries s = new TimeSeries("F0");
        double x = 0;
        for (double frekv : paFrekvencie) {
            if (frekv != 0) {
                s.addOrUpdate(new FixedMillisecond((long) x), frekv);
            } else {
                s.addOrUpdate(new FixedMillisecond((long) x), null);
            }
            x += casOkna;
        }
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        dataset.addSeries(s);
        return dataset;
    }

    private void vytvorChart() {
        DateAxis da = (DateAxis) chart.getXYPlot().getDomainAxis();
        da.setDateFormatOverride(new SimpleDateFormat("mm:ss.SSS"));
        chart.getXYPlot().setDomainPannable(true);
        chart.getXYPlot().setRangePannable(true);
        tonovaOs = new ToneAxis("Tóny", Keyboard.getPoleTonov());
        ciselnaOs = (NumberAxis) chart.getXYPlot().getRangeAxis();
        ciselnaOs.setRange(0, tonovaOs.getFrekvNajvyssiehoTonu() + tonovaOs.getFrekvNajvyssiehoTonu() * 0.1);
        XYStepRenderer r = new XYStepRenderer();
        chart.getXYPlot().setRenderer(r);

        ChartPanel cp = new ChartPanel(chart);
        cp.setMouseWheelEnabled(true);
        pnlPlatno.setLayout(new BorderLayout());
        pnlPlatno.add(cp, BorderLayout.CENTER);
        pnlPlatno.revalidate();
        repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        rbtnTony = new javax.swing.JRadioButton();
        rbtnFrekvencia = new javax.swing.JRadioButton();
        pnlPlatno = new javax.swing.JPanel();
        lblZhoda = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Graf");
        setExtendedState(6);

        buttonGroup1.add(rbtnTony);
        rbtnTony.setText("Tones");
        rbtnTony.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnTonyActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbtnFrekvencia);
        rbtnFrekvencia.setSelected(true);
        rbtnFrekvencia.setText("Frequency");
        rbtnFrekvencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnFrekvenciaActionPerformed(evt);
            }
        });

        pnlPlatno.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout pnlPlatnoLayout = new javax.swing.GroupLayout(pnlPlatno);
        pnlPlatno.setLayout(pnlPlatnoLayout);
        pnlPlatnoLayout.setHorizontalGroup(
            pnlPlatnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pnlPlatnoLayout.setVerticalGroup(
            pnlPlatnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 510, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlPlatno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(rbtnTony)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rbtnFrekvencia)
                        .addGap(18, 18, 18)
                        .addComponent(lblZhoda, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 319, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rbtnTony)
                        .addComponent(rbtnFrekvencia))
                    .addComponent(lblZhoda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlPlatno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rbtnTonyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnTonyActionPerformed
        Range rng = chart.getXYPlot().getRangeAxis().getRange();
        tonovaOs.setRange(rng);
        chart.getXYPlot().setRangeAxis(tonovaOs);
    }//GEN-LAST:event_rbtnTonyActionPerformed

    private void rbtnFrekvenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnFrekvenciaActionPerformed
        Range rng = chart.getXYPlot().getRangeAxis().getRange();
        ciselnaOs.setRange(rng);
        chart.getXYPlot().setRangeAxis(ciselnaOs);
    }//GEN-LAST:event_rbtnFrekvenciaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel lblZhoda;
    private javax.swing.JPanel pnlPlatno;
    private javax.swing.JRadioButton rbtnFrekvencia;
    private javax.swing.JRadioButton rbtnTony;
    // End of variables declaration//GEN-END:variables
}
