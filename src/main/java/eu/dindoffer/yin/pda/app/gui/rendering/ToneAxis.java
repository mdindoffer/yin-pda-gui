package eu.dindoffer.yin.pda.app.gui.rendering;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTick;
import org.jfree.chart.axis.Tick;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.data.Range;
import eu.dindoffer.yin.pda.app.util.Tone;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.chart.ui.TextAnchor;

/**
 * Os grafu s hudobnými tónmi.
 *
 * @author Martin Dindoffer
 */
public class ToneAxis extends NumberAxis {

    private final Tone[] poleTonov;

    /**
     * Vráti frekvenciu najvyššieho tónu osi.
     *
     * @return frekvencia najvyššieho tónu
     */
    public double getFrekvNajvyssiehoTonu() {
        return poleTonov[poleTonov.length - 1].getFrekvencia();
    }

    /**
     * Vytvorí os s daným názvom a množinou tónov.
     *
     * @param paNazov názov osi
     * @param paPoleTonov pole tónov pre hodnoty osi.
     */
    public ToneAxis(String paNazov, Tone[] paPoleTonov) {
        super(paNazov);
        poleTonov = paPoleTonov;
    }

    /**
     * Vypočíta index najnižšieho tónu, ktorý je viditeľný pri aktuálnom zobrazení.
     *
     * @return index najnižšieho viditeľného tónu
     */
    private int calculateLowestVisibleTickIndex() {
        double podlaha = Math.ceil(getRange().getLowerBound());
        for (int i = 0; i < poleTonov.length; i++) {
            if (poleTonov[i].getFrekvencia() > podlaha) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Vypočíta počet tónov ktoré sú viditeľné pri aktuálnom zobrazení.
     *
     * @return počet viditeľných tónov
     */
    @Override
    protected int calculateVisibleTickCount() {
        Range range = getRange();
        double strop = Math.floor(range.getUpperBound());
        double podlaha = Math.ceil(getRange().getLowerBound());
        Math.ceil(range.getLowerBound());
        int indexMax = -1;
        int indexMin = -1;
        for (int i = 0; i < poleTonov.length; i++) {
            if (poleTonov[i].getFrekvencia() < strop) {
                indexMax = i;
            }
        }
        for (int i = 0; i < poleTonov.length; i++) {
            if (poleTonov[i].getFrekvencia() > podlaha) {
                indexMin = i;
                break;
            }
        }
        if (indexMax != -1 && indexMin != -1) {
            return indexMax - indexMin + 1;
        } else {
            return 0;
        }
    }

    /**
     * Vráti zoznam značiek tónov, ktoré sú viditeľné pri aktuálnom zobrazení.
     *
     * @param g2 grafický kontext
     * @param dataArea plocha, kde sa má vykreslovať
     * @param edge umiestnenie osi
     * @return zoznam značiek tónov
     */
    @Override
    protected List refreshTicksVertical(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {
        Font tickLabelFont = getTickLabelFont();
        g2.setFont(tickLabelFont);

        int count = calculateVisibleTickCount();
        int lowestTickIndex = calculateLowestVisibleTickIndex();

        List result = new java.util.ArrayList();
        if (count <= ValueAxis.MAXIMUM_TICK_COUNT) {
            for (int i = 0; i < count; i++) {
                double currentTickValue = poleTonov[lowestTickIndex + i].getFrekvencia();
                String tickLabel;
                tickLabel = poleTonov[lowestTickIndex + i].getLabel();

                double angle = 0.0;
                TextAnchor anchor = TextAnchor.CENTER_RIGHT;
                TextAnchor rotationAnchor = TextAnchor.CENTER_RIGHT;

                Tick tick = new NumberTick(currentTickValue, tickLabel, anchor, rotationAnchor, angle);
                result.add(tick);
            }
        }
        return result;
    }
}
