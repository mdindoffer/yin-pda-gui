package eu.dindoffer.yin.pda.app.util;

/**
 * Zoznam 12 tónov oktávy
 * @author Martin Dindoffer
 */
public enum ToneNames {

    C("C"), CSHP("C#"), D("D"), DSHP("D#"), E("E"), F("F"), FSHP("F#"), G("G"), GSHP("G#"), A("A"), ASHP("A#"), B("B");

    private final String nazovTonu;

    private ToneNames(String paNazov) {
        this.nazovTonu = paNazov;
    }
    
    @Override
    public String toString(){
        return nazovTonu;
    }
}
